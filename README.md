
## Name
Portfolio

## Description
Ce portfolio a pour objectif de me présenter à d'éventuels recruteurs. 
Dans un premier temps, je me présente. Je propose ma CV en pdf de manière à pouvoir le télécharger si besoin.
Ensuite je liste les langages informatiques que j'espère savoir utiliser d'ici le stage.
Par la suite, j'expose mes projets réalisés avec les différents languages cités au dessus.
Enfin, je mets à dispositions mes différents réseaux sociaux si le visiteur souhaite me contacter 

## Add your files

git remote add origin https://gitlab.com/Melanie1234/portfolio.git
git branch -M main
git push -uf origin main
 
la maquette de mon projet : https://www.figma.com/file/tsd8jkG03aYbO07O0t3VB1/Untitled?node-id=1%3A2&t=jh4gBJQyH6mVpXZa-0

## Usage
Ce projet mobilise les languages HTML et CSS et le framework Boostrap. Il est consultable sur https://gitlab.com/Melanie1234/portfolio.git
Une fois le projet proche de son aboutissement, je l'ai déployé via Netlify cf. https://peaceful-pudding-14a835.netlify.app/

## Support
En cas d'interrogations ou de difficultés de navigation, il est possible de me contacter à l'adresse : melanie.ferer@hotmail.fr

## Project status
Ce portfolio est en cours car nous l'agrémenterons tout au long de la formation avec nos différents projets.
Aussi, toutes suggestions ou remarques sont les bienvenues.
